import Swal from 'sweetalert2'
(function(){
  emailjs.init("user_lDntRVlrxqhPoB5QnMf7X");

  let sendButton = document.getElementById('send-button')
  let emailField = document.getElementById('email-field')
  let messageField = document.getElementById('message-field')

  sendButton.addEventListener('click', () => {
    let email = emailField.value
    let message = messageField.value
    emailField.classList.remove('input-error')
    messageField.classList.remove('input-error')
    if (validate(email, message)) {
      emailjs.send("hola_sense_aid", "template_yl5FX829", {
        'from_name': email,
        'message_html': message
      })
      window.location.href = '#'
      emailField.value = ''
      messageField.value = ''
      Swal.fire(
        'Mensaje enviado',
        'Nos pondremos en contacto lo antes posible',
        'success'
      )
    }
  })

  function validate(email, message) {
    let isValid = true
    if (!validateEmail(email)) {
      isValid = false
      emailField.classList.add('input-error')
    }
    if (message === '') {
      isValid = false
      messageField.classList.add('input-error')
    }
    return isValid
  }
  function validateEmail(email) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(email).toLowerCase())
  }
})();
